![tzfzf](.assets/tzfzf.webp)

# tzfzf

<img src=".assets/demo.gif">

Sift through timezones and their respective times.
Rewrite of [timezone.sh](https://gitlab.com/chanceboudreaux/timezone.sh) in Python.

## Dependencies

- Python
- [pytz](https://pythonhosted.org/pytz/)
- [fzf](https://github.com/junegunn/fzf)

## Installation

- Manual way:

```
git clone https://codeberg.org/theooo/tzfzf.git
cd tzfzf/
sudo make install
```

or

```
sudo curl -sL "https://codeberg.org/theooo/tzfzf/raw/branch/main/tzfzf" -o /usr/local/bin/tzfzf
sudo chmod +x /usr/local/bin/tzfzf
```

## Usage

```
usage: tzfzf [-h] [-d]

Sift through timezones and their respective times.

options:
  -h, --help    show this help message and exit
  -d , --date   Custom date based on which times of different timezones would
                be generated.
```
